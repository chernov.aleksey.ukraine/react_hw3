import React, { useEffect, useState } from "react";
import "./App.scss";
import ItemList from "./Components/ItemList/ItemList";
import Header from "./Components/Header/Header";
import Modalwindow from "./Components/Modalwindow/Modalwindow";
import { Routes, Route } from "react-router-dom";
import CartList from "./Components/CartList/CartList";
import FavoriteList from "./Components/FavoriteList/FavoriteList";
let counter = 0;

const App = () => {
  const [array, setArray] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [chosenItem, setChosenItem] = useState({});

  const addToCart = (art) => {
    setArray((current) => {
      const array = [...current];
      const index = array.findIndex((el) => el.art === art);

      if (index !== -1) {
        array[index].isCart = true;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return array;
    });
  };

  const handleClick = (value) => {
    setIsModalOpen(value);
  };
  const chooseItem = (value) => {
    setChosenItem(value);
  };

  const emptyFunc = () => {};

  const addToFavor = (card) => {
    setArray((current) => {
      const array = [...current];
      const index = array.findIndex((el) => el.art === card.art);
      if (index !== -1) {
        array[index].isFavorite = true;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return array;
    });
  };

  const moveFromCart = (art) => {
    setArray((current) => {
      const array = [...current];
      const index = array.findIndex((el) => el.art === art);
      if (index !== -1) {
        array[index].isCart = false;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return array;
    });
  };
  const moveFromFavor = (card) => {
    setArray((current) => {
      const array = [...current];
      const index = array.findIndex((el) => el.art === card.art);
      if (index !== -1) {
        array[index].isFavorite = false;
      }
      localStorage.setItem("array", JSON.stringify(array));
      return array;
    });
  };
  const favorCount = ({ array }) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isFavorite) {
        counter += 1;
      }
    });
    return counter;
  };
  const cartCount = ({ array }) => {
    counter = 0;
    array.forEach((el) => {
      if (el.isCart) {
        counter += 1;
      }
    });
    return counter;
  };

  useEffect(() => {
    const getData = async () => {
      if (localStorage.getItem("array")) {
        setArray(JSON.parse(localStorage.getItem("array")));
      } else {
        setArray(await fetch(`./items.json`).then((res) => res.json()));
      }
    };
    getData();
  }, []);

  return (
    <div className="App">
      {!isModalOpen && (
        <Header array={array} favorCount={favorCount} cartCount={cartCount} />
      )}
      <Routes>
        <Route path="/" element={
            <>
              <ItemList
                array={array}
                addToFavor={addToFavor}
                moveFromCart={emptyFunc}
                moveFromFavor={moveFromFavor}
                chooseItem={chooseItem}
                handleClick={handleClick}
              />
              <Modalwindow
                isModalOpen={isModalOpen}
                operateCart={addToCart}
                handleClick={handleClick}
                chosenItem={chosenItem}
                headertext={"Adding item to the cart."}
                maintext1={"Do you want to add"}
                maintext2={"to the cart?"}
              />
            </>
          }
        />
        <Route path="/favorite" element={
            <>
              <FavoriteList
                array={array}
                addToFavor={addToFavor}
                moveFromCart={emptyFunc}
                moveFromFavor={moveFromFavor}
                chooseItem={chooseItem}
                handleClick={handleClick}
              />
              <Modalwindow
                isModalOpen={isModalOpen}
                operateCart={addToCart}
                handleClick={handleClick}
                chosenItem={chosenItem}
                headertext={"Adding item to the cart."}
                maintext1={"Do you want to add"}
                maintext2={"to the cart?"}
              />
            </>
          }
        />
        <Route path="/cart" element={
            <>
              <CartList
                array={array}
                addToFavor={addToFavor}
                moveFromCart={moveFromCart}
                moveFromFavor={moveFromFavor}
                chooseItem={chooseItem}
                handleClick={handleClick}
              />
              <Modalwindow
                isModalOpen={isModalOpen}
                operateCart={moveFromCart}
                handleClick={handleClick}
                chosenItem={chosenItem}
                headertext={"Removing item from the cart."}
                maintext1={"Do you want to remove"}
                maintext2={"from the cart?"}
              />
            </>
          }
        />
      </Routes>
    </div>
  );
};

export default App;
