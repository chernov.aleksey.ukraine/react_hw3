import React from "react";
import CartItem from "../CartItem/CartItem";
import "./CartList.scss";
import PropTypes from "prop-types";

const CartList = ({
  array,
  addToFavor,
  moveFromCart,
  moveFromFavor,
  handleClick,
  chooseItem,
}) => {
  return (
    <div className="itemcontainer">
          {array.map(({ name, art, color, price, url, isFavorite, isCart }) => (
               isCart ?  <CartItem
            key={art}
            name={name}
            art={art}
            color={color}
            price={price}
            url={url}
            isFavorite={isFavorite}
            isCart={isCart}
            addToFavor={addToFavor}
            moveFromCart={moveFromCart}
            moveFromFavor={moveFromFavor}
            handleClick={handleClick}
            chooseItem={chooseItem}
          /> : null 
      
      ))}
    </div>
  );
};
CartList.propTypes = {
  moveFromFavor: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  moveFromCart: PropTypes.func.isRequired,
  addToFavor: PropTypes.func.isRequired,
  chooseItem: PropTypes.func.isRequired,
  array: PropTypes.array.isRequired,
};
export default CartList;
