import React from "react";
import "./Header.scss";
import PropTypes from "prop-types";
import Navigation from "../Navigation/Navigation";

const Header = ({ array, favorCount, cartCount }) => {
  return (
    <div className="headercountcontainer">
      <Navigation array={array} favorCount={favorCount} cartCount={cartCount} />
    </div>
  );
};
Header.propTypes = {
  favorCount: PropTypes.func.isRequired,
  cartCount: PropTypes.func.isRequired,
  array: PropTypes.array.isRequired,
};
export default Header;
