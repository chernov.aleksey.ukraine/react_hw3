import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

const Navigation = ({ array, favorCount, cartCount }) => {
  return (
    <nav>
       
        
          <NavLink to="/">Home Page</NavLink>
        
          <NavLink to="/favorite">Favorites( {favorCount({ array })} )</NavLink>
         
          <NavLink to="/cart">Cart( {cartCount({ array })} )</NavLink>
         
    </nav>
  );
};
Navigation.propTypes = {
  favorCount: PropTypes.func.isRequired,
  cartCount: PropTypes.func.isRequired,
  array: PropTypes.array.isRequired,
};
export default Navigation;
