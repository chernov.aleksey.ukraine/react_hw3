import React from "react";
import "./CartItem.scss";
import PropTypes from "prop-types";

const CartItem = ({
  moveFromFavor,
  addToFavor,
  name,
  art,
  color,
  price,
  url,
  isFavorite,
  chooseItem,
  handleClick,
}) => {
  return (
    <div className="itemcard">
      <div className="itemcardheader">
        <p className="itemart">code: {art}</p>
        <div className="iconcontainer">
          {isFavorite ? (
            <p
              onClick={() => {
                moveFromFavor({ art });
              }}
            >
              &#9733;
            </p>
          ) : (
            <p
              onClick={() => {
                addToFavor({ art });
              }}
            >
              &#9734;
            </p>
          )}
          <div className="carticonholder">
            <p
              onClick={() => {
                chooseItem({ art, name });

                handleClick(true);
              }}
            >REMOVE(&#10006;)</p>
          </div>
        </div>
      </div>
      <div className="itemphotocontainer">
        <img src={url} alt="img" />
      </div>
      <p className="itemname">{name}</p>
      <p className="itemcolor">color: {color}</p>
      <p className="itemprice">
        {price} <span>гр.</span>
      </p>
    </div>
  );
};

CartItem.propTypes = {
  moveFromFavor: PropTypes.func.isRequired,
  addToFavor: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  art: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool.isRequired,
  chooseItem: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
};
export default CartItem;

