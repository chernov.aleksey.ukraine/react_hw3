import React from "react";
import "./Modalwindow.scss";
import Button from "../Button/Button";
import PropTypes from "prop-types";

const Modalwindow = ({
      isModalOpen,
      handleClick,
      chosenItem,
      operateCart,
      headertext,
      maintext1,
      maintext2,
    })=> {
  
    if (!isModalOpen) return null;

    return (
      <>
        <div
          className="globalback"
          onClick={() => {
            handleClick(false);
          }}
        >
          <div className="modal">
            <header className="modalheader">
              <p>{headertext}</p>

              <div
                className="crossbutton"
                onClick={() => {
                  handleClick(false);
                }}
              >
                &#10006;
              </div>
            </header>
            <main className="modalmain">
              <div>
                {maintext1} "{chosenItem.name}" {maintext2}
              </div>
            </main>
            <div className="modalbuttoncontainer">
              <Button
                onClick={() => {
                  operateCart(chosenItem.art);
                  handleClick(false);
                }}
                text={"OK"}
              />
              <Button
                onClick={() => {
                  handleClick(false);
                }}
                text={"CLOSE"}
              />
            </div>
          </div>
        </div>
      </>
    );
  
}
Modalwindow.propTypes = {
  headertext: PropTypes.string.isRequired,
  maintext1: PropTypes.string.isRequired,
  maintext2: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
  operateCart: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  chosenItem: PropTypes.shape({
    name: PropTypes.string,
    art: PropTypes.number,
  }).isRequired,
};

export default Modalwindow;
